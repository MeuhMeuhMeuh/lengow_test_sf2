<?php
namespace FakeCompany\Bundle\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Yaml\Yaml;

/**
 *
 * Very tiny-smally-thin layer for test APIs handling stuff. :D
 *
 * @author Alexis M.
 * @since 09/08/2015
 */
class ApiController extends Controller
{
    // API default output format.
    const DEFAULT_OUTPUT_FORMAT = 'json';

    // Silly format bindings with content types.
    public static $contentTypes = [
        'json' => 'application/json',
        'yml' => 'text/yaml',
        'yaml' => 'text/yaml',
        'html' => 'text/html',
        'xml' => 'text/xml'
    ];

    /**
     * Formats a response according to the current environment.
     * 
     * @param  mixed $data The data to return in the created response.
     *
     * @return Response A proper Response object according to the current environment.
     */
    protected function getApiResponse($data)
    {
        $responseFormat = $this->determineResponseFormat();

        return $this->buildResponse($data, $responseFormat);
    }

    /**
     * Builds a response according to the format.
     * This method is definitely not generic since we are handling two specific formats.
     * For our test, it's fine but building a real way to implement different format contexts would be
     * greater in an real environment.
     *
     * @param  mixed $data   The data to insert into the Response.
     * @param  string $format The relevant format (e.g. 'json', 'html'...).
     *
     * @return Response
     */
    protected function buildResponse($data, $format)
    {
        $response = new Response();

        // We assume that the data is already formatted.
        $response->setContent($data, $format);
        $response->headers->set('Content-Type', self::$contentTypes[$format]);

        return $response;
    }

    /**
     * Determines the response format, according to the current environment.
     *
     * @return string
     */
    protected function determineResponseFormat()
    {
        $request = $this->get('request_stack')->getCurrentRequest();

        return $this->getFormatParameterFromRequest($request) ?: self::DEFAULT_OUTPUT_FORMAT;
    }

    /**
     * Retrieves, according to the current request, the API format parameter.
     *
     * @param  Request $request
     *
     * @return string|null The format parameter, if any.
     */
    protected function getFormatParameterFromRequest(Request $request)
    {
        return 'GET' === $request->server->get('REQUEST_METHOD') ?
            $request->query->get('_format') :
            $request->request->get('_format');
    }

    /**
     * Returns a dummy non-generic notfound response.
     *
     * @return Response
     */
    protected function getApiNotFound()
    {
        $format = $this->determineResponseFormat();

        $response = new Response();
        $response->setStatusCode(Response::HTTP_NOT_FOUND);

        $response->headers->set('Content-Type', self::$contentTypes[$format]);

        return $response;
    }

    /**
     * Quick-fix for a "YAML" format instead of a "YML"-attended format when calling an API.
     * This is JUST for the test environment and is NOT generic.
     *
     * @param  string $format
     *
     * @return A serializer-safe format.
     */
    protected function getSerializerFormat($format)
    {
        return 'yaml' === $format ? 'yml' : $format;
    } 
}