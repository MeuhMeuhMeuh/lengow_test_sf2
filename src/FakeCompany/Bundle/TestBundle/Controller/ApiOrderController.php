<?php
namespace FakeCompany\Bundle\TestBundle\Controller;

use FakeCompany\Bundle\TestBundle\Entity\Order;
use Symfony\Component\HttpFoundation\Response;

/**
 *
 * API Controller for the orders management.
 *
 * @author Alexis M.
 * @since 09/08/2015
 */
class ApiOrderController extends ApiController
{
    /**
     * Retrieving every order.
     *
     * @return Response A list of every order, in the appropriate format.
     */
    public function getAllAction()
    {
        $entityManager = $this->get('doctrine')->getEntityManager();

        $orders = $entityManager->getRepository('FakeCompanyTestBundle:Order')
            ->findAll();

        $responseFormat = $this->determineResponseFormat();

        $serializedOrders = $this->get('jms_serializer')->serialize($orders, $this->getSerializerFormat($responseFormat));

        return $this->getApiResponse($serializedOrders);
    }

    /**
     * Retrieving an order thanks to its INTERNAL id, and NOT the order_id field from the XML flow.
     *
     * @return Response
     */
    public function getAction($orderId)
    {
        $entityManager = $this->get('doctrine')->getEntityManager();

        $order = $entityManager->getRepository('FakeCompanyTestBundle:Order')
            ->find($orderId);

        if (!$order) {
            $this->getApiNotFound();
        } 

        $responseFormat = $this->determineResponseFormat();

        $serializedOrder = $this->get('jms_serializer')->serialize($order, $this->getSerializerFormat($responseFormat));

        return $this->getApiResponse($serializedOrder);
    }
}
