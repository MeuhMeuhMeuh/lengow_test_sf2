<?php
namespace FakeCompany\Bundle\TestBundle\Controller;

use FakeCompany\Bundle\TestBundle\Entity\Order;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use APY\DataGridBundle\Grid\Source\Entity;

/**
 *
 * Controller dealing with the homepage interactions.
 *
 * @author Alexis M.
 * @since 07/08/2015
 */
class HomeController extends Controller
{
    /**
     * Homepage controller.
     *
     * @return Response
     */
    public function indexAction()
    {
        $xmlParser = $this->get('lengow_test');

        $xml = $xmlParser->parse();

        $m = $this->get('fake_company_test.order_manager');

        $orders = $m->parseOrdersFromXml($xml);

        $persistedOrdersNumber = $m->persistOrders($orders);

        // Building the grid stuff.
        $source = new Entity('FakeCompanyTestBundle:Order');
        $grid = $this->get('grid');
        $grid->setSource($source);

        if ($grid->isReadyForRedirect()) {
            return $grid->getGridResponse();
        } 

        return $this->render('FakeCompanyTestBundle:Home:index.html.twig', [
            'persistedOrdersNumber' => $persistedOrdersNumber,
            'grid' => $grid
        ]);
    }
}
