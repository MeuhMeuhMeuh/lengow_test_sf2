<?php
namespace FakeCompany\Bundle\TestBundle\Controller;

use FakeCompany\Bundle\TestBundle\Entity\Order;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * Controller dealing with the orders management.
 *
 * @author Alexis M.
 * @since 09/08/2015
 */
class OrderController extends Controller
{
    /**
     * Add form.
     *
     * @return Response
     */
    public function newAction(Request $request)
    {

        $order = new Order();

        /*
         * For simplicity sake, we create the form here.
         * For best practices, we would have created an AbstractType class.
         * For the currency and amount field, we could have built a "currency" field too.
         */
        $form = $this->createFormBuilder($order)
            ->add('orderId', 'text')
            ->add('marketplace', 'text')
            ->add('orderItems', 'integer')
            ->add('currency', 'choice', [
                'choices' => ['EUR' => 'Euro', 'USD' => 'Dollar'] // For testing only
            ])
            ->add('amount', 'number', [
                'precision' => 2
            ])
            ->add('add', 'submit')
            ->getForm();

        $form->handleRequest($request);

        /*
         * For simplicity again, we handle the form submission directly in the same controller action.
         * In another environment, we would handle it in a separate resource.
         */
        if ($form->isValid()) {
            $entityManager = $this->get('doctrine')->getEntityManager();

            $entityManager->persist($order);

            $entityManager->flush();
            
            // Redirecting to the homepage.
            return $this->redirect($this->generateUrl('fake_company_test_homepage'));
        }

        return $this->render('FakeCompanyTestBundle:Order:new.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
