<?php
namespace FakeCompany\Bundle\TestBundle\Entity;

use APY\DataGridBundle\Grid\Mapping as GRID;
use Doctrine\ORM\Mapping as ORM;

/**
 * Order entity.
 * Note : I just persisted a few information and used simple types to simplify the process (e.g. to me, a currency
 * should be a specific entity, such as a marketplace, etc).
 * 
 * Note 2 : we prefix the order table with "lengow_" in order to prevent from SQL queries issues ("order").
 *
 * @author Alexis M.
 * @since 07/08/2015
 * 
 * @ORM\Entity
 * @ORM\Table(
 *     name="lengow_order",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="order_id_idx", columns={"order_id"})}
 * )
 * @GRID\Source(columns="id, orderId, marketplace, orderItems, currency, amount")
 */
class Order
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * The order ID. I assume that this is one of the fields coming from the XML flow that are unique.
     *
     * @var integer
     *
     * @ORM\Column(type="string", unique=true)
     */
    private $orderId;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $marketplace;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    private $orderItems;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $currency;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=10, scale=4)
     */
    private $amount;

   /**
    * Getter id.
    *
    * @return integer
    */
    public function getId()
    {
        return $this->id;
    }
     
   /**
    * Setter id.
    *
    * @param integer $id
    *
    * @return self
    */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
 
   /**
    * Getter orderId.
    *
    * @return integer
    */
    public function getOrderId()
    {
        return $this->orderId;
    }
     
   /**
    * Setter orderId.
    *
    * @param integer $orderId
    *
    * @return self
    */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }
 
   /**
    * Getter marketplace.
    *
    * @return string
    */
    public function getMarketplace()
    {
        return $this->marketplace;
    }
     
   /**
    * Setter marketplace.
    *
    * @param string $marketplace
    *
    * @return self
    */
    public function setMarketplace($marketplace)
    {
        $this->marketplace = $marketplace;

        return $this;
    }
 
   /**
    * Getter currency.
    *
    * @return string
    */
    public function getCurrency()
    {
        return $this->currency;
    }
     
   /**
    * Setter currency.
    *
    * @param string $currency
    *
    * @return self
    */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }
 
   /**
    * Getter amount.
    *
    * @return float
    */
    public function getAmount()
    {
        return $this->amount;
    }
     
   /**
    * Setter amount.
    *
    * @param float $amount
    *
    * @return self
    */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }
 
   /**
    * Getter orderItems.
    *
    * @return integer
    */
    public function getOrderItems()
    {
        return $this->orderItems;
    }
     
   /**
    * Setter orderItems.
    *
    * @param integer $orderItems
    *
    * @return self
    */
    public function setOrderItems($orderItems)
    {
        $this->orderItems = $orderItems;

        return $this;
    }
}