<?php
namespace FakeCompany\Bundle\TestBundle\Manager;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use FakeCompany\Bundle\TestBundle\Entity\Order;
use Psr\Log\LoggerInterface;
use Symfony\Component\DomCrawler\Crawler;

/**
 *
 * Order's manager handling business logic related to orders.
 * 
 * Note : we could have used the Serializer component to provide a corresponding XML format to the Order entity.
 * For the sake of simplicity, we use the DomCrawler component to parse the XML and create simple partial Order entities.
 * 
 * Note 2 : in a real environment, we would have separated the service in charge of dealing with the XML <=> Orders interactions
 * from the one persisting and dealing with the DBAL <=> Orders interactions.
 *
 * @author Alexis M.
 * @since 08/08/2015
 */
class OrderManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    /**
     * Persists a non-persisted set of Order entities.
     * This does NOT update potential already-existing orders.
     *
     * @param  Collection $collection An Order entities Doctrine Collection.
     *
     * @return integer The number of orders that have been persisted.
     */
    public function persistOrders(Collection $collection)
    {
        // Retrieving the existing order identifiers that are supposed to represent an unique order.
        // This is for simplicity and performance sake : we get all the order Ids and won't persist
        // any order that has the same id. In a "production" environment, we'd prefer a way to update them.
        $existingOrderIds = $this->getPersistedOrderIds();

        $persistedOrdersNumber = 0;

        try {
            foreach($collection as $order) {
                if (!in_array($order->getOrderId(), $existingOrderIds, true)) {
                    $this->entityManager->persist($order);
                    $persistedOrdersNumber++;
                }
                // @todo Update :-).
            }

            $this->entityManager->flush();
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());

            return $persistedOrdersNumber;
        }
        
        return $persistedOrdersNumber;
    }

    /**
     * Retrieves an array of all existing order ids in Database.
     * 
     * Note : this code could have been in a custom Order Repository.
     *
     * @return array A flat array with all the order IDs.
     */
    public function getPersistedOrderIds()
    {
        $qb = $this->entityManager
            ->getRepository('FakeCompanyTestBundle:Order')
            ->createQueryBuilder('o');
        $qb
            ->select('o.orderId');

        $result = $qb->getQuery()->getArrayResult();

        $flatOrderIds = [];
        array_walk_recursive($result, function($orderId) use (&$flatOrderIds) {
            $flatOrderIds[] = $orderId;
        });

        return $flatOrderIds;
    }

    /**
     * Parses some orders from a corresponding XML.
     *
     * @param  mixed $xml The XML content. This can be an array of nodes or a string, whatever the Crawler accepts.
     *
     * @return ArrayCollection A collection of non-persisted Orders.
     */
    public function parseOrdersFromXml($xml)
    {
        $crawler = new Crawler($xml);

        $crawler = $crawler->filter('statistics > orders')->children();

        $orders = new ArrayCollection();

        $crawler->each(function($node) use($orders) {
            try {
                $orders->add($this->buildOrderFromCrawler($node));
            } catch(\Exception $e) {
                // We suppose that the orders HAVE to be represented with every field in the XML.
                $this->logger->error(sprintf(
                    'Could not instanciate an order according to the related XML. Exception message : "%s". Node : %s',
                    $e->getMessage(),
                    $node->text()
                ));
            }
            
        });

        return $orders;
    }

    /**
     * From a Crawler instance, builds a corresponding Order entity.
     *
     * @param  Crawler $crawler
     *
     * @return Order
     */
    public function buildOrderFromCrawler($crawler) {
        $order = new Order();

        $order->setMarketplace($crawler->filter('order > marketplace')->text());
        $order->setOrderId($crawler->filter('order > order_id')->text());
        $order->setOrderItems(intval($crawler->filter('order > order_items')->text()));
        $order->setCurrency($crawler->filter('order > order_currency')->text());
        $order->setAmount(intval($crawler->filter('order > order_amount')->text()));

        return $order;
    }
}
