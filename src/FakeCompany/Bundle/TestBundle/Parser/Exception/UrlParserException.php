<?php
namespace FakeCompany\Bundle\TestBundle\Parser\Exception;

/**
 *
 * An exception typically thrown by an URL Parser.
 *
 * @author Alexis M.
 * @since 07/08/2015
 */
class UrlParserException extends \Exception
{
    /**
     * Not supported URL parser exception.
     *
     * @param string $url
     *
     * @return self
     */
    public static function unsupportedUrlException($url)
    {
        return new self(sprintf(
            'The provided URL is not supported by the given parser. URL : %s',
            $url
        ));
    }

    /**
     * Unable to get any content exception.
     *
     * @param string $url
     *
     * @return self
     */
    public static function noContentException($url)
    {
        return new self(sprintf(
            'The URL parser could not retrieve any content from the URL. URL : %s',
            $url
        ));
    }
}