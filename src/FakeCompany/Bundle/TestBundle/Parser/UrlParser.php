<?php
namespace FakeCompany\Bundle\TestBundle\Parser;

use FakeCompany\Bundle\TestBundle\Parser\Exception\UrlParserException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints\Url as UrlConstraint;

/**
 *
 * URL Parser interface for consistency purpose.
 *
 * @author Alexis M.
 * @since 07/08/2015
 */
abstract class UrlParser
{
    /**
     * XML Parser's logger.
     *
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Symfony validator instance.
     *
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * Parses an URL and returns the parsed data.
     *
     * @param string $url The URL to parse. If url is null, then the class implementing
     * this one should have its own way to get the URL.
     *
     * @return mixed
     */
    abstract protected function parseUrl($url);

    /**
     * Should indicate whether the corresponding URL is supported by the parser.
     *
     * @return boolean
     */
    abstract public function supports($url);

    /**
     * A getter for the URL to be parsed.
     *
     * @return string
     */
    abstract public function getUrl();


    public function __construct(LoggerInterface $logger, ValidatorInterface $validator)
    {
        $this->logger = $logger;
        $this->validator = $validator;
    }

    /**
     * Parses an URL and returns the corresponding parsed data.
     * 
     * @api
     * 
     * @see UrlParser::parseUrl()
     *
     * @throws UrlParserException When the provided URL is not supported by the parser.
     *
     * @return mixed
     */
    public function parse()
    {
        $url = $this->getUrl();

        if (!$this->supports($url) || !$this->validateUrl($url))
        {
            $this->logger->error(sprintf(
                'A parsing operating failed : The given URL "%s" is not supported by the parser "%s".',
                $url,
                get_class($this)
            ));
            throw UrlParserException::unsupportedUrlException($url);
        }

        return $this->parseUrl($url);
    }

    /**
     * Validates an URL according to the validator.
     *
     * @param  string $url
     *
     * @return boolean
     */
    private function validateUrl($url)
    {
        $urlConstraint = new UrlConstraint();

        $errors = $this->validator->validate($url, $urlConstraint);

        return (0 === count($errors));
    }




    // GETTERS / SETTERS
    
    /**
     * Getter logger.
     *
     * @return LoggerInterface
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
    * Setter logger.
    *
    * @param LoggerInterface $logger
    *
    * @return self
    */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;

        return $this;
    }

   /**
    * Getter validator.
    *
    * @return ValidatorInterface
    */
    public function getValidator()
    {
        return $this->validator;
    }
     
   /**
    * Setter validator.
    *
    * @param ValidatorInterface $validator
    *
    * @return self
    */
    public function setValidator(ValidatorInterface $validator)
    {
        $this->validator = $validator;

        return $this;
    }
}