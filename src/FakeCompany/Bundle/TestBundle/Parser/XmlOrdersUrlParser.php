<?php
namespace FakeCompany\Bundle\TestBundle\Parser;

use FakeCompany\Bundle\TestBundle\Parser\Exception\UrlParserException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\ValidatorInterface;

/**
 *
 * XML Parser dedicated for the fakecompany orders.
 *
 * @author Alexis M.
 * @since 07/08/2015
 */
class XmlOrdersUrlParser extends UrlParser
{
    // Base domain to restrict given URLs. Used as a dummy test :-).
    // This constant could also be in the config.yml configuration file...
    const ORDERS_URL_DOMAIN = 'test.lengow.io';

    /**
     * Orders' XML URL in which are detailed the orders.
     *
     * @var string
     */
    private $ordersUrl;


    public function __construct($ordersUrl, ValidatorInterface $validator, LoggerInterface $logger)
    {
        $this->ordersUrl = $ordersUrl;

        parent::__construct($logger, $validator);
    }

    /**
     * {@inheritdoc}
     */
    public function supports($url)
    {
        // Dummy test to check for a simple authorized domain.
        $host = parse_url($url, PHP_URL_HOST);

        return ($host === self::ORDERS_URL_DOMAIN);
    }

    /**
     * {@inheritdoc}
     */
    public function getUrl()
    {
        return $this->getOrdersUrl();
    }

    /**
     * {@inheritdoc}
     */
    public function parseUrl($url)
    {
        // @todo
        return $this->getUrlContent($url);
    }

    /**
     * Retrieves an URL resource content.
     * Note : this process uses a very simple way to get the file content, because it's a test focused on a specific file.
     *
     * @param  string $url
     *
     * @throws UrlParserException When the URL content could not be retrieved.
     * 
     * @return string The content is returned as a string.
     */
    private function getUrlContent($url)
    {
        $content = @file_get_contents($url); // Special @-prefixed because the error handling is just after.
        if (false === $content) {
            $this->getLogger()->error(sprintf('Could not parse the content from the specified URL %s.', $url));
            throw UrlParserException::noContentException($url);
        }

        $this->getLogger()->info(sprintf('Successfully parsed content from the URL %s.', $url));

        return $content;
    }

    // GETTERS / SETTERS

    /**
     * Getter ordersUrl.
     *
     * @return string
     */
    public function getOrdersUrl()
    {
        return $this->ordersUrl;
    }
      
    /**
     * Setter ordersUrl.
     *
     * @param string $ordersUrl
     *
     * @return self
     */
    public function setOrdersUrl($ordersUrl)
    {
        $this->ordersUrl = $ordersUrl;

        return $this;
    }
}

